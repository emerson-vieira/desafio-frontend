export default function formatName(value: string) {
    return value.charAt(0).toUpperCase() + value.slice(1).split("-").join(" ");
}