import { format, parseISO } from 'date-fns';
export default function formatDate(date: any): string{
    if(!date) return "";
    return format(parseISO(date), "dd/MM/yyyy");
}