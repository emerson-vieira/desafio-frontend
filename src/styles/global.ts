import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    :root {
        --primary: #8C56C2;
        --dark-primary: #784AA6;
        --light-accent: #A99FAA;
        --dark-accent: #7B6490;
        --light-shades: #F1F1EF;
        --dark-shades: #2F2544;
        --white: #fff;
        --black: #000;

    }

    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    @media(max-width: 1080px) {
        html {
            font-size: 93.75%; /* 1rem == 15px */
        }
    }

    @media(max-width: 720px) {
        html {
            font-size: 87.5%; /* 1rem == 14px */
        }
    }

    body {
        background: var(--light-shades);
        color: var(--light-accent);
    }

    body {
        font: 400 1rem "Roboto", sans-serif;   
    }

    button {
        cursor: pointer;
    }

    a {
        color: inherit;
        text-decoration: none;
    }

    form {
        display: flex;
        flex-wrap: wrap;
        
        width: 100%;
    }
`