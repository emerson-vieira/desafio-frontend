import { Switch, Route } from "react-router-dom";

import { Home } from "../pages/Home";
import Results from "../pages/Results";

function Router(){
    return (
        <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/results" component={Results} />
        </Switch>
    );
}

export default Router;