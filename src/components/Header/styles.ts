import styled from "styled-components";


export const Container = styled.header`
    display: flex;
    flex-direction: row;
    align-items: center;
    
    width: 100%;
    height: 3.5rem;

    padding: 0.625rem 2.5rem;
    background: var(--white);
    box-shadow: 0px 4px 4px rgba(47, 37, 68, 0.15);
`;


export const Logo = styled.div<{src: any, alternativeLogo: any}>`
    width: 100%;
    max-width: 50px;
    height: 100%;

    background-size: contain;
    background-repeat: no-repeat;
    background-image: url(${({ alternativeLogo }) => alternativeLogo});

    a {
        display: block;
        width: 100%;
        height: 100%;
    }

    @media (min-width: 576px) {
        max-width: 100px;
        background-image: url(${({ src }) => src});
    }
`;

export const WrapperInput = styled.div`
    display: flex;
    flex-direction: row;

    width: 100%;
    margin-left: 10px;

    input {
        width: 100%;
        height: 2.25rem;
        
        padding: 0.5rem;
        border-radius: 4px 0 0 4px;
        border: 1px solid #E1DFE0;
        outline: 0;

        &:focus {
            border-color: var(--primary);
        }
    }

    button {
        width: 2.25rem;
        height: 2.25rem;

        box-shadow: -2px 0px 4px rgba(0, 0, 0, 0.25);
        border-radius: 0px 4px 4px 0px;
        border: 0;
        outline: 0;
        background: var(--primary);
        color: var(--white);
        transition: background-color .5s;
        
        &:hover {

            background: var(--dark-primary);
        }
    }
`;