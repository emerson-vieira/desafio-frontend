import { FormEvent, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { FaSearch } from "react-icons/fa";

import { useSearch } from "../../context/SearchContext";

import { Container, Logo, WrapperInput } from "./styles";

import Logotype from "../../assets/Logotype.svg"
import LogoResposive from "../../assets/Logo.svg"

export function Header() {
    const [search, setSearch] = useState("");
    let { push } = useHistory();
    const { handleSearchUsers } = useSearch();

    function handleSearch(event: FormEvent) {
        event.preventDefault();
        if(!search) return;
        handleSearchUsers(search);
        push("/results");
    }

    return (
        <Container>
            <Logo src={Logotype} alternativeLogo={LogoResposive} >
                <Link to="/" />
            </Logo>
            <form onSubmit={handleSearch}>
                <WrapperInput>
                    <input 
                        type="text" 
                        placeholder="Pesquisar"
                        value={search}
                        onChange={data => setSearch(data.target.value)}
                    />
                    <button type="submit">
                        <FaSearch />
                    </button>
                </WrapperInput>
            </form>
        </Container>
    );
}