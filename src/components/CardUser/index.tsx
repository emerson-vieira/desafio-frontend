import formatName from "../../util/formatName";

import { Container, WrapperInfos, Button } from "./styles";

interface PropsCardUser {
    data: UsersData;
    onSubscribe: (login: string) => void;
}

interface UsersData {
    id: number;
    login: string;
    avatar_url: string;
    html_url: string;
    score: number;
}

export function CardUser({ data, onSubscribe }: PropsCardUser) {
    const { login, avatar_url, html_url, score } = data;
    return (
        <Container>
            <img src={avatar_url} alt={login} />
            <WrapperInfos>
                <section>
                    <strong>{formatName(login)}</strong>
                    <a href={html_url} target="_blank" rel="noreferrer">
                        <p>{html_url}</p>
                    </a>
                    <p className="score">{`Score: ${score.toFixed(2)}`}</p>
                </section>
                <section>
                    <Button onClick={() => onSubscribe(login)}>VER MAIS</Button>
                </section>
            </WrapperInfos>
        </Container>
    );
}