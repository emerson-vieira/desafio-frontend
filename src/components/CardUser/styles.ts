import styled from "styled-components";


export const Container = styled.div`
    display: flex;
    flex-direction: column;
    
    width: 230px;
    height: 273px;

    background: var(--white);
    box-shadow: 0px 0px 6px rgba(47, 37, 68, 0.25);
    border-radius: 10px;
    overflow: hidden;

    > img {
        width: 100%;
        height: 130px;
        object-fit: cover;
    }

`;

export const WrapperInfos = styled.div`
    height: 100%;
    padding: 1.25rem 0.625rem;
    display: grid;
    grid-template-rows: repeat(2, 1fr);
    align-items: center;

    section {
        strong {
            font-size: 1.125rem;
            font-weight: normal;
            line-height: 21px;
            color: var(--dark-shades);
        }

        a {
            color: #03CADA;
            transition: color 0.3s;
            
            &:hover {
                color: #03caff;
            }
        }

        p {
            margin-top: 4px;
            font-size: 0.75rem;
            line-height: 14px;
        }

        p.score{
            color: var(--dark-shades);
        }
    }


`;


export const Button = styled.button`
    width: 100%;
    height: 2.25rem;

    margin-top: 10px;
    border-radius: 20px;
    background: var(--primary);
    color: var(--white);
    border: 0;
    outline: 0;
    transition: background-color .5s;
        
    &:hover {
        background: var(--dark-primary);
    }
`;