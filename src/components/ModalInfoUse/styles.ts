import styled from "styled-components";


interface PropsCol {
    textAlign?: string;
}

interface PropsRow {
    justifyContent?: string;
}

export interface PropsContainer {
    open: boolean;
} 

export const Container = styled.div<PropsContainer>`
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;

    display: ${({open}) => open ? "flex": "none"};
    align-items: center;
    justify-content: center;

    background: rgba(47, 37, 68, 0.2);
    backdrop-filter: blur(4px);

    z-index: 10;
`;

export const Modal = styled.section`
    display: grid;
    grid-template-columns: 266px auto;
    gap: 1.25rem;


    width: 100%;
    max-width: 980px;
    height: 346px;
    padding: 2.5rem;

    background: var(--white);
    box-shadow: 0px 0px 6px rgba(47, 37, 68, 0.25);
    border-radius: 10px;

    z-index: 11;

    img {
        width: 100%;
        border-radius: 10px;
    }

    @media (max-width: 1200px) {
        margin: 0 1.5rem
    }

    @media (max-width: 720px) {
        grid-template-columns: 1fr;
        grid-template-rows: repeat(2, 1fr);
        gap: 1rem;

        height: auto;

        img {
        //max-height: calc(0.7 * 100vw);
            max-height: 266px;
        }
    }

    @media (max-width: 576px) {
        img {
            max-height: calc(0.7 * 100vw);
        }
    }
`;


export const WrapperInfo = styled.div`
    h1 {
        width: 100%;
        font-size: 1.125rem;
        font-weight: normal;
        line-height: 21px;
        color: var(--dark-shades);

        padding: 0.625rem 0;
        border-bottom: 1px solid #E1DFE0;
    }
`;

export const Row = styled.div<PropsRow>`
    display: flex;
    flex-direction: row;
    justify-content: ${({justifyContent}) => justifyContent || "flex-start"};

    width: 100%;
    margin-top: 1.25rem;
`;

export const Col = styled.div<PropsCol>`
    display: grid;
    grid-template-rows: repeat(2, 1fr);

    font-size: 0.875rem;
    line-height: 16px;

    strong {
        font-weight: normal;
        color: var(--dark-shades);
    }

    p {
        color: var(--dark-accent);
        text-align: ${({textAlign}) => textAlign ||  "left"};
    }
`;

export const Button = styled.button`
    border-radius: 4px;
    border: 1px solid var(--primary);
    padding: 0.625rem 1rem;
    background: transparent;

    font-size: 0.875rem;
    font-weight: 500;
    line-height: 1rem;
    text-transform: uppercase;
    color: var(--primary);
`;