import { useEffect, useState } from "react";

import apiGitHub from "../../services/github";
import formatDate from "../../util/formatDate";

import { Container, Modal, WrapperInfo, Row, Col, Button } from "./styles";

export interface PropsModal {
    open: boolean;
    idUser?: number;
    onCloseModal: (open : boolean) => void;
} 

interface PropsUser {
    name: string;
    login: string;
    avatar_url: string;
    html_url: string;
    followers: number;
    following: number;
    created_at: Date;
}

export function ModalInfoUser({ open, idUser, onCloseModal }: PropsModal) {
    const [user, setUser] = useState({} as PropsUser);

    useEffect(() => {
        (async () => {
            if(open) {
                const { data } = await apiGitHub.get(`/users/${idUser}`);
                setUser(data);
            }
        })()
    }, [idUser, open]);

    function handleCloseModal(){
        setUser({} as PropsUser);
        onCloseModal(false);   
    }

    return (
        <Container open={open}>
            <Modal>
                <img src={user.avatar_url} alt={user.name} />
                <WrapperInfo>
                    <h1>{user.name}</h1>
                    <Row justifyContent="space-between">
                        <Col>
                            <strong>Username:</strong>
                            <p>{user.login}</p>
                        </Col>
                        <Col textAlign="end">
                            <strong>Seguindo:</strong>
                            <p>{user.following}</p>
                        </Col>
                    </Row>
                    <Row justifyContent="space-between">
                        <Col>
                            <strong>Cadastrado(a):</strong>
                            <p>{formatDate(user.created_at)}</p>
                        </Col>
                        <Col textAlign="end">
                            <strong>Seguidores:</strong>
                            <p>{user.followers}</p>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <strong>URL:</strong>
                            <a href={user.html_url} target="_blank" rel="noreferrer">
                                <p>{user.html_url}</p>
                            </a>
                        </Col>
                    </Row>
                    <Row justifyContent="flex-end">
                        <Button onClick={handleCloseModal}>FECHAR</Button>
                    </Row>
                </WrapperInfo>
            </Modal>
        </Container>
    );
}

export const withModalInfoUser = (Component: any) => (props: any)=> {
    const [open, setOpen] = useState(false);
    const [idUser, setIdUser] = useState();
    return (
        <>
            <ModalInfoUser open={open} idUser={idUser} onCloseModal={setOpen} />
            <Component {...props} onModal={setOpen} userSelected={setIdUser} />      
        </>
    )
}