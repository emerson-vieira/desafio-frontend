import { useSearch } from "../../context/SearchContext";
import { CardUser } from "../CardUser";
import { Pagination } from "../Pagination";
import { withModalInfoUser } from "../ModalInfoUse";

import { Container, WrapperCards } from "./styles";

import Home from "../../assets/Logotype-home.svg";
import Background from "../../assets/Background.svg";

interface PropsBody {
    onModal: (open: boolean) => void;
    userSelected: (login: string) => void;
}

const Body = ({ onModal, userSelected }: PropsBody) => {
    const { users, page, numberOfPages, handleChangePage } = useSearch();

    function handleInfoUser(login: string){
        userSelected(login);
        onModal(true);
    }



    return (
        <Container style={{
            backgroundImage: `url(${users.length > 0 ? Background : ""})`
        }}>
            {users.length > 0 ? (
                <section>
                    <h1>Resultados para: Paytime</h1>
                    <WrapperCards>
                        {users.map(user => (
                            <CardUser key={user.id} data={user} onSubscribe={handleInfoUser} />
                        ))}
                    </WrapperCards>
                    <Pagination page={page} numberOfPages={numberOfPages} onPage={handleChangePage}/>
                </section>
            ) : (
                <img src={Home} alt="Home" />
            )}
        </Container>
    );
}

export default withModalInfoUser(Body);

// backgroundImage: `url("https://via.placeholder.com/500")`