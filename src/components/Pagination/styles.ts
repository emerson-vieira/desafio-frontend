import styled from "styled-components";

export const Container = styled.footer`
    display: flex;
    flex-direction: row;
    justify-content: center;
    width: 100%;
    height: 2.5rem;

    button {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 2rem;
        height: 2rem;
        border: 0;
        outline: 0;
        font-size: 1.2rem;
        font-weight: bold;
        background: transparent;
        color: var(--primary);
    }

    button.isDisabled {
        cursor: not-allowed;
        opacity: 0.5;
    }

    span {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 2rem;
        height: 2rem;
        cursor: default;
    }
`;