import { FaAngleDoubleLeft, FaAngleDoubleRight } from "react-icons/fa";

import { Container } from "./styles";

interface PaginationProps {
    page: number;
    numberOfPages: number;
    onPage: (numberPage: number) => void;
}

export function Pagination({ page, numberOfPages, onPage}: PaginationProps){

    function handlePage(numberPage: number){
        onPage(numberPage)   
    } 

    return (
        <Container>
            <button
                disabled={page === 1 ? true : false}
                className={`${page === 1 ? "isDisabled" : ""}`}
                onClick={() => handlePage(page - 1)}
            >
                <FaAngleDoubleLeft />
            </button>
            <span>{page}</span>
            <button
                disabled={numberOfPages === 1 || page === numberOfPages ? true : false}
                className={`${numberOfPages === 1 || page === numberOfPages ? "isDisabled" : ""}`}
                onClick={() => handlePage(page + 1)}
            >
                <FaAngleDoubleRight />
            </button>
        </Container>
    );
}