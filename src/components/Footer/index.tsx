import { Container } from "./styles";

export function Footer() {
    return (
        <Container>
            <p>Projetado por: Fulano de Tal - 31/12/2020</p>
        </Container>
    );
}