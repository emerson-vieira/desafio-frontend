import styled from "styled-components";


export const Container = styled.footer`
    display: flex;
    justify-content: center;
    align-items: center;
    
    width: 100%;
    height: 2.25rem;

    padding: 0.625rem 2.5rem;
    background: var(--white);
    box-shadow: 0px -4px 4px rgba(47, 37, 68, 0.15);


    p {
        font-size: 0.875rem;
        line-height: 16.41px;
        color: var(--dark-accent);
    }
`;