
import { Container } from "./styles";

import HomeImage from "../../assets/Logotype-home.svg";

export function Home() {
    return (
        <Container >
            <img src={HomeImage} alt="Home" />
        </Container>
    );
}