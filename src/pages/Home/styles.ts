import styled from "styled-components";


export const Container = styled.main`   
    display: flex;
    justify-content: center;
    align-items: flex-start;

    padding: 2.5rem;

    width: 100%;
    height: calc(100vh - 3.5rem - 2.25rem);

    overflow: hidden;

    > img {
        width: 80vw;
        max-width: 730px;
        max-height: 410px;
    }
`;