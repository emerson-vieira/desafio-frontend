import { FaArrowLeft } from "react-icons/fa";
import { useHistory } from "react-router-dom";

import { CardUser } from "../../components/CardUser";
import { Pagination } from "../../components/Pagination";
import { useSearch } from "../../context/SearchContext";
import { withModalInfoUser } from "../../components/ModalInfoUse";

import { Container, WrapperCards, Navbar } from "./styles";

import Background from "../../assets/Background.svg";

interface PropsResults {
    onModal: (open: boolean) => void;
    userSelected: (login: string) => void;
}

const Results = ({ onModal, userSelected }: PropsResults) =>  {
    const { users, page, numberOfPages, handleChangePage } = useSearch();
    let { goBack } = useHistory();
    
    function handleInfoUser(login: string){
        userSelected(login);
        onModal(true);
    }

    return (
        <Container style={{
            backgroundImage: `url(${Background})`
        }}>
            <section>
                <Navbar>
                    <button onClick={() => goBack()}>
                        <FaArrowLeft />
                    </button>
                    <h1>Resultados para: Paytime</h1>
                </Navbar>
                <WrapperCards>
                    {users.map(user => (
                        <CardUser key={user.id} data={user} onSubscribe={handleInfoUser} />
                    ))}
                </WrapperCards>
                <Pagination page={page} numberOfPages={numberOfPages} onPage={handleChangePage}/>
            </section>
        </Container>
    );
}

export default withModalInfoUser(Results);