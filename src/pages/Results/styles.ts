import styled from "styled-components";


export const Container = styled.main`   
    display: flex;
    justify-content: center;
    align-items: flex-start;

    padding: 2.5rem;

    width: 100%;
    height: calc(100vh - 3.5rem - 2.25rem);
    
    overflow-y: scroll;
    overflow-x: hidden;

    > img {
        width: 80vw;
        max-width: 730px;
        max-height: 410px;
    }

    > section {
        display: flex;
        flex-direction: column;
        justify-content: flex-start;
        align-items: flex-start;
        
        width: 100%;
        max-width: 980px;

        h1 {
            width: 100%;
            font-size: 1.125rem;
            font-weight: normal;
            line-height: 21px;
            color: var(--dark-shades);
        }
    }
`;


export const WrapperCards = styled.div`
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    gap: 20px;

    width: 100%;
    padding: 1.25rem 0;
`;


export const Navbar = styled.header`
    display: flex;
    flex-direction: row;
    align-items: center;

    width: 100%;
    padding: 0.625rem 0;
    border-bottom: 1px solid #E1DFE0;

    button {
        display: flex;
        justify-content: center;
        align-items: center;
        
        width: 1.5rem;
        height: 1.5rem;

        background: transparent;
        border: 0;
        outline: 0;

        font-size: 1.125rem;
        color: var(--dark-shades);
    }
`;