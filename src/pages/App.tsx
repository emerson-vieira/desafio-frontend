import {BrowserRouter as Router} from "react-router-dom";
import { SearchProvider } from "../context/SearchContext";

import { Header } from "../components/Header";
import { Footer } from "../components/Footer";
import Routes from "../routes";

import Global from "../styles/global";

function App() {
    return (
        <Router>
            <Global />
            <SearchProvider>
                <Header />
                <Routes />
            </SearchProvider>
            <Footer />
        </Router>
    );
}

export default App;
