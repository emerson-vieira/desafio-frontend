import { createContext, ReactNode, useContext, useEffect, useState } from "react";
import apiGitHub from "../services/github";

interface PropsSearchProvider {
    children: ReactNode
}

interface PropsSearchContext {
    users: UsersData[];
    page: number;
    numberOfPages: number;
    handleSearchUsers: (value: string) => void;
    handleChangePage: (page: number) => void;
}

interface UsersData {
    id: number;
    login: string;
    avatar_url: string;
    html_url: string;
    score: number;
}

const SearchContext = createContext({} as PropsSearchContext);


function SearchProvider({ children }: PropsSearchProvider){
    const [users, setUsers] = useState([]);
    const [search, setSearch] = useState("");
    const [page, setPage] = useState(1);
    const [numberOfPages, setNumberOfPages] = useState(1);
    const per_page = 8;

    useEffect(() => {
        (async () => {
            if(!search) return; 
            try {
                const { data } = await apiGitHub.get(`/search/users?q=${search}&per_page=${per_page}&page=${page}`);
                let calcNumberOfPages = Math.ceil(data.total_count / per_page) || 0;
                setUsers(data.items);
                setNumberOfPages(calcNumberOfPages);
            } catch (error) {
                alert(error.message);         
            }
        })()
    }, [search, page]);


    function handleSearchUsers(value: string){
        setPage(1);
        setSearch(value);
    }

    function handleChangePage(page: number){
        setPage(page);
    }

    return (
        <SearchContext.Provider value={{
            users,
            page,
            numberOfPages,
            handleSearchUsers,
            handleChangePage
        }}>
            {children}
        </SearchContext.Provider>
    );
}

function useSearch(){
    return useContext(SearchContext);
}

export {
    SearchProvider,
    useSearch
}