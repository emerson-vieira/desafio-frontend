## Desafio frontend

[![Author](https://img.shields.io/badge/Author-Emerson%20Vieira-blue)](https://github.com/emerson-vieira)
[![Languages](https://img.shields.io/badge/Languages-1-blue)](#)


## 🧪 Technologies

This project was developed using the following technologies:

- [React](https://reactjs.org)
- [TypeScript](https://www.typescriptlang.org/)
- [Styled Component](https://styled-components.com/)

## 🚀 Getting started

### Requirements

- You need to install both [Node.js](https://nodejs.org/en/download/) and [Yarn](https://yarnpkg.com/) to run this project.

**Clone the project and access the folder**

```bash
$ git clone https://emerson-vieira@bitbucket.org/emerson-vieira/desafio-frontend.git && cd desafio-frontend
```

**Follow the steps below**
```bash
# Install the dependencies
$ npm install or yarn
# Run the web server
$ npm run start or yarn start
```

The app will be available for access on your browser at `http://localhost:3000`

## 🔖 Layout

You can view the project layout through the links below:

- [Layout Web](https://www.figma.com/file/bAxYmnMBHqmHL9SY6YfDmG/Internship-Test?node-id=14%3A4009) 

Remembering that you need to have a [Figma](http://figma.com/) account to access it.